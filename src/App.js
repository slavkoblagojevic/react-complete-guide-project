import React, { useState } from 'react';

import AddUser from './components/Users/AddUser';
import UsersList from './components/Users/UsersList';

function App() {
  const [users, setUsers] = useState([]);
  
  const addUser = (userName, userAge) => {
    setUsers((prevUsers) => {
      return(
        [{id: Math.random(), name:userName, age: userAge}, ...prevUsers]
      )
    });
    console.log(userName, userAge);
  }

  return (
    <div>
      <AddUser onAddUser={addUser}/>
      <UsersList users={users} />
    </div>
  );
}

export default App;
