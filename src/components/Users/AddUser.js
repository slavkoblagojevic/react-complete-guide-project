import React, { useState } from "react";
import classes from "./AddUser.module.css";
import Card from "../UI/Card";
import Button from "../UI/Button";
import ErrorModal from "../UI/ErrorModal";

const AddUser = (props) => {
  const [enterdUsername, setUsername] = useState("");
  const [enterdAge, setAge] = useState("");
  const [isValidUsername, setIsValidUsername] = useState(true);
  const [isValidAge, setIsValidAge] = useState(true);
  const [error, setError] = useState(null);

  const addUserHandler = (event) => {
    event.preventDefault();

    if (enterdUsername.length === 0) {
      setIsValidUsername(false);
    }

    if (enterdUsername.trim().length === 0 || enterdAge.trim().length === 0) {
      setError({
        title: "Invalid input!",
        message: "Username and age can not be empty, age must be > 0",
      });
      return;
    }

    if (enterdAge < 1) {
      setIsValidAge(false);
      setError({ title: "Invalid age!", message: "Set age positive and greater then 0" });
      return;
    }

    props.onAddUser(enterdUsername, enterdAge);
    console.log(enterdUsername, enterdAge);
    setUsername("");
    setAge("");
  };

  const usernameHandler = (event) => {
    if (event.target.value.length > 0) {
      setIsValidUsername(true);
    }
    setUsername(event.target.value);
  };

  const ageHandler = (event) => {
    if (event.target.value.trim() > 0) {
      setIsValidAge(true);
    }
    setAge(event.target.value);
  };

  const errorHandler = () => {
    setError(null);
  };

  return (
    <div>
      {error && (
        <ErrorModal
          title={error.title}
          message={error.message}
          onConfirm={errorHandler}
        />
      )}
      <Card className={classes.input}>
        <form onSubmit={addUserHandler}>
          <label
            style={{ color: !isValidUsername ? "red" : null }}
            htmlFor="username"
          >
            Username
          </label>
          <input
            style={{ borderColor: !isValidUsername ? "red" : null }}
            id="username"
            type="text"
            value={enterdUsername}
            onChange={usernameHandler}
          ></input>
          <label style={{ color: !isValidAge ? "red" : null }} htmlFor="age">
            Age (Years)
          </label>
          <input
            style={{ borderColor: !isValidAge ? "red" : null }}
            id="age"
            type="number"
            value={enterdAge}
            onChange={ageHandler}
          ></input>
          <Button type="submit">Add User</Button>
        </form>
      </Card>
    </div>
  );
};

export default AddUser;
